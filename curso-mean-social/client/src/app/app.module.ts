import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { routing, appRoutingProviders} from './app.routin.module';
import { FormsModule } from '@angular/forms';
import { HttpClientModule} from '@angular/common/http';

// Componentes:
import { AppComponent } from './app.component';
import { LoginComponent } from './components/login/login.component';
import { RegisterComponent } from './components/register/register.component';

@NgModule({
  declarations: [
    // aqui puedo colocar directivas, componentes y pipes:
    AppComponent,
    LoginComponent,
    RegisterComponent
  ],
  imports: [
    BrowserModule,
    FormsModule ,
    // Instancia de la clase o modulo
    routing,
    HttpClientModule
  ],
  providers: [
    // servicios de manera global:
    appRoutingProviders
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
