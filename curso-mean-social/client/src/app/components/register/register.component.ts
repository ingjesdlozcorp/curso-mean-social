import { Component, OnInit } from '@angular/core';
import {Router, ActivatedRoute, Params} from '@angular/router';
import { User } from 'src/app/models/user';
import { UserService } from '../../services/user.service'


@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css'],
  providers: [ UserService ]
})
export class RegisterComponent implements OnInit {
  public title: String
  public user: User;
  public status: string;
  constructor(
    private _route: ActivatedRoute,
    private _router: Router,
    private _userService: UserService  ) {
    this.title ='Registrate';
    this.user = new User("", "", "", "", "", "", "ROLE_USER", "");

   }

  ngOnInit() {
    console.log('Componente de register cargando...');
  }
  onSubmit(form){
    console.log(this.user);
    this._userService.register(this.user).subscribe(
      response => {
        if (response.status !== 404 &&  response.status !== 500) {
          console.log(response.body);
          this.status = 'success';
          form.reset;
        } else {
          this.status = 'error';
        }
      },
      error => {
        console.log(error);
        this.status = 'error';
      }
    );
  }

}
