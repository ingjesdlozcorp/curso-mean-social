import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';
import {GLOBAL} from './global';
import { User } from '../models/user';


@Injectable({
  providedIn: 'root'
})
export class UserService {
  public url: string;

  constructor(public _http: HttpClient) {
    this.url = GLOBAL.url;
  }

  register(user: User) {
    const params: string = JSON.stringify(user);
    console.log(params);
    const headers = new HttpHeaders().set('Conten-Type', 'application/json');

    // return this._http.post(this.url + 'register', params, {headers: 'headers'}); no me sirvio como el lo hace
    return this._http.post(this.url + 'register', user, {observe: 'response'}); // sacado de green USF
  }
}
