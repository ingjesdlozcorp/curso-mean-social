import {ModuleWithProviders} from '@angular/core';
import { Routes, RouterModule} from '@angular/router';

import { LoginComponent } from './components/login/login.component';
import { RegisterComponent } from './components/register/register.component';

const appRoutes = [
    {path: '', component: LoginComponent},
    {path: 'login', component: LoginComponent},
    {path: 'registro', component: RegisterComponent}
];
// Servicio:
export const appRoutingProviders: any[] = [];
// modulo o instancia:
export const routing: ModuleWithProviders = RouterModule.forRoot(appRoutes);