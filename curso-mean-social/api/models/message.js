'use strict'

//cargar el modulo de mongoose:
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var MessageSchema = Schema({
    text: String,
    viewed: String,
    created_at: String,
    meiter: { type: Schema.ObjectId, ref:'User' },
    recieber: { type: Schema.ObjectId, ref:'User' }
});

module.exports = mongoose.model('Message', MessageSchema)