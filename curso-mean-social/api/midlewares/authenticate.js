'use strict'

// importar modulo jwt:
var jwt = require('jwt-simple');
// importar modulo moment:
var moment = require ('moment');
// creamos una clave secreta que solo conocemos los desarrolladores del backend:
var secret = 'clave_secreta_curso_desarrollar_red_social_angular';

exports.ensureAuth = function(req, res, next){
    if(!req.headers.authorization){
        return res.status(403).send({message: 'La petició de la cabezera no tiene autenticacion'});
    }
    // limpiamos el token:
    var token = req.headers.authorization.replace(/['"]+/g, '');
    
    try {

        var payload = jwt.decode(token, secret);
        if(payload.exp <= moment.unix()){
            return res.status(401).send({
                message: 'El token ha expirado'
            });
        } 

    } catch (error) {

        return res.status(404).send({
            message: 'El token no es valido'
        });
    }
    // creamos una propiedad en caliente:
    req.user = payload;
    next();
}