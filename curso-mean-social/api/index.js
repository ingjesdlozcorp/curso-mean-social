// End point principal para lanzar el backen:
'use strict'
// cargar la libreria de mongoose:
var mongoose = require('mongoose');
// importar el modulo app:
var app = require('./app');
// indicamos un puerto:
var port = 3800;
//conexion a dataBase:
mongoose.Promise = global.Promise;
mongoose.connect('mongodb://localhost:27017/curso_mean_social', {useMongoClient: true})
    .then(() =>{
        console.log('La conexion a la base de datos: curso_mean_social se ha realizado correctamente!!')

        // Crear server:
        app.listen(port, ()=> {
            console.log("Sevidor corriendo en http:// localhost:3800");
        });

    })
    .catch(err=> console.log( err));
