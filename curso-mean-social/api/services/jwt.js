'use strict'

// importar modulo jwt:
var jwt = require('jwt-simple');
// importar modulo moment:
var moment = require ('moment');
// creamos una clave secreta que solo conocemos los desarrolladores del backend:
var secret = 'clave_secreta_curso_desarrollar_red_social_angular';

exports.createToken = function(user){
    var payload = {
        sub: user._id,
        name: user.name,
        surname: user.surname,
        nick: user.nick,
        email: user.email,
        role: user.role,
        image: user.image,
        iat: moment().unix(),
        exp: moment().add(30, 'days').unix()
    };

    // luego de creado el payload, se procede a generar el token:
    return jwt.encode(payload, secret);

};