'use strict'

//importo mongoose-paginate:
var mongoosePaginate = require('mongoose-pagination');
// importo los modelos:
var Follow = require('../models/follow');
var User = require('../models/user');


// seguir un usuario:
function saveFollow(req, res){
   var params = req.body;

   var follow = new Follow();
   follow.user = req.user.sub;
   follow.followed = params.followed;

   follow.save((err, followStored) => {
        if(err) return res.status(500).send({
            message: 'Error al guardar el documento'
        });

        if(!followStored) return res.status(404).send({
            message: 'El seguimiento no se ha guardado'
        });

        return res.status(200).send({follow:followStored});
   });


}

// dejar de seguir:
function deleteFollow(req, res){
    var userId = req.user.sub;
    var followId = req.params.id;

    // Consulta:
    Follow.find({'user': userId, 'followed': followId}).remove(err => {
        if(err) return res.status(500).send({
            message: 'Error al dejar de seguir'
        });

        return res.status(200).send({message: 'El folow se ha eliminado'});
    });
}

//Devolver listado  de usuarios que sigo sigo:
function getFollowingUsers(req, res){
    var userId = req.user.sub;

    if(req.params.id && req.params.page){
        userId =  req.params.id;
    }

    var page = 1;

    if(req.params.page){
        page = req.params.page;
    }
    var itemsPerPage = 4;

    // Consulta:

    Follow.find({user: userId}).populate({path: 'followed'})
        .paginate(page, itemsPerPage, (err, follows, total) => {

            if(err) return res.status(500).send({
                message: 'Error en el servidor'
            });

            if(!follows) return res.status(404).send({
                message: 'No hay  estas siguiendo a ningun usuario'
            });

            return res.status(200).send({
                total: total,
                pages: Math.ceil(total/itemsPerPage),
                follows
            });
    });
}

// Lista de seguidores:
function getFollowedUsers(req, res){
    var userId = req.user.sub;

    if(req.params.id && req.params.page){
        userId =  req.params.id;
    }

    var page = 1;

    if(req.params.page){
        page = req.params.page;
    }else{
        page = req.params.id;
    }
    var itemsPerPage = 4;

    // Consulta:

    Follow.find({followed: userId}).populate('user')
        .paginate(page, itemsPerPage, (err, follows, total) => {

            if(err) return res.status(500).send({
                message: 'Error en el servidor'
            });

            if(!follows) return res.status(404).send({
                message: 'No te sigue ningun usuario'
            });

            return res.status(200).send({
                total: total,
                pages: Math.ceil(total/itemsPerPage),
                follows
            });
    });
}
//Listados no paginados

// Devolver listado usuarios:
function getMyFollows(req, res){
    var userId = req.params.sub;
    
    var find = Follow.find({user: userId});

    if(req.params.followed){
        find = Follow.find({Followed: userId});
    }

    Follow.find({user: userID}).populate('user followed').exec((err, follows) =>{
        if(err) return res.status(500).send({
            message: 'Error en el servidor'
        });

        if(!follows) return res.status(404).send({
            message: 'No  sigues ningun usuario'
        });

        return res.status(200).send({
            follows
        });

    });
}



module.exports = {
    saveFollow,
    deleteFollow,
    getFollowingUsers,
    getFollowedUsers,
    getMyFollows
   
}

