var path = require('path');
var fs = require('fs');
var moment = require('moment');
var mongoosePaginate = require ('mongoose-pagination');

var Publication  = require('../models/publication');
var User  = require('../models/user');
var Follow  = require('../models/follow');

function probando(){
    console.log(probando);
}


function savePublication(req, res){
    var params = req.body;

    if(!params.text) return res.status(200).send({ message: 'Debes enviar un texto!!'});

    var publication = new Publication();
    publication.text = param.tex;
    publication.file = 'null';
    publication.user = req.user.sub;
    publication.created_at = moment().unix();

    publication.save((err, publicationStored)=>{
        if(err) return res.status(500).send({message:'Error al guardar la publicación'});

        if(!publicationStored) return res.status(404).send({message:'La publicación No ha sido guardada'});

        return res.status(200).send({publicatión: publicationStored});

    });

}

function getPublications(req, res){
    var page = 1;
    if(req.params.page){
        page = req.params.page;
    }
    var itemsPerpage = 4

    Follow.find({user: req.user.sub}).populate('followed').exec((err, follows) =>{
        if(err) return res.status(500).send({message: 'Error al devolver el seguimiento'});

        var follows_clean = [];
        
        follows.forEach((follow)=>{
            follows_clean.push(follow.followed);
        });

        Publication.find({user:{"$in":follows_clean}}).sort('created_at')
        .populate('user').paginate(page, itemsPerpage, (err, publications, total)=>{
            if(err) return res.status(500).send({message: 'Error al devolver publicaciones'});

            if(!publications) return res.status(404).send({message: 'No hay publicaciones '});

            return res.status(200).send({
                total_items: total,
                pages: Math.ceil(total/itemsPerpage),
                page:page,
                publications: publications
            });

        });
    });
}

function getPublication(req, res){
    var publicationId = req.params.id;

    Publication.findById(publicationId, (err, publication) => {
        if(err) return res.status(500).send({message: 'Error al devolver publicacion'});

        if(!publication) return res.status(404).send({message: 'No existe la publicacion'});

        return res.status(200).send({publication});
    });
}

function deletePublication(req, res){
    var publicationId = req.params.id;

    Publication.find({'user': req.user.sub, '_id': publicationId}).remove(err => {
        if(err) return res.status(500).send({message: 'Error al borar la publicacion'});

       // if(!publicationRemoved) return res.status(404).send({message: 'No no se ha borrado la publicacion'});

        return res.status(200).send({message: 'Publication removed'});
    });
}

function uploadImage(req, res){
    var publicationId = req.params.id;

   
    if(req.files){
        var file_path = req.files.image.path;
        console.log(file_path);

        var file_split = file_path.split('\\');
        console.log(file_split);

        var file_name = file_split[2];
        console.log(file_name);
        
        var ext_split = file_name.split('\.');
        console.log(ext_split);
        var file_ext = ext_split[1];
        console.log(file_ext);


       

        if(file_ext == 'png' || file_ext == 'jpg' ||  file_ext == 'jpeg' ||  file_ext == 'gif'){

         publication.findOne({'user':req.user.sub, '_id':publicationId}).exec((err, publication)=>{
            if(publication){
                Publication.findByIdAndUpdate(publicationId, {file: file_name}, {new:true}, (err, publicationUpdated) => {
                    if(err) return res.status(500).send({ message: 'Error en la petición'});
            
                    if(!publicationUpdated){
                         return res.status(404).send({
                             message: 'No se ha podido actualizar la publicación'
                        });
                    }
            
                    return res.status(200).send({publication: publicationUpdated});
                });
            }else{
                return removeFilesOfUploads(res, file_path, 'Sin permiso para actualizar publicacion');
            }
         });
           
            
        }else{
           return removeFilesOfUploads(res, file_path, 'Extension no válida');
        }

   }else{
    return res.status(200).send({ message: 'No se han subido imagenes'});  

   }
 
}

function removeFilesOfUploads(res, file_path, message) {
    fs.unlink(file_path, (err) => {
        return res.status(200).send({
            message: message
       });
    });
}

// Devolver imagen de un usuario:
function getImageFile(req, res){
    var image_file = req.params.imageFile;
    var path_file = './uploads/publications/'+image_file;

    fs.exists(path_file, (exists) => {
        if(exists){
            res.sendFile(path.resolve(path_file));
        }else{
            res.status(200).send({message:'No existe la imagen...'})
        }
    });
}







module.exports = {
    probando,
    savePublication,
    getPublications,
    getPublication,
    deletePublication,
    uploadImage,
    removeFilesOfUploads,
    getImageFile
}
 