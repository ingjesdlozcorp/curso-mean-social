'use strict'
// importante para cifrar la contraseña:
var bcrypt = require('bcrypt-nodejs');
// importo el modelo:
var User = require('../models/user');
var Follow = require('../models/follow');
var Publication = require('../models/publication');
//importo el servicio:
var jwt = require('../services/jwt');
//importo mongoose-paginate:
var mongoosePaginate = require('mongoose-pagination');

//para trabajar con ficheros:
var fs  = require('fs');
var path = require('path');

// metodos de prueba:
function home(req, res){
    res.status(200).send({
        message:'Hola'
    });
}

function pruebas(req, res){
    res.status(200).send({
        message:'Accion de prubas en el sever'
    });
}
// registro
function saveUser(req, res){
    //instancia del modelo de usuario:
    var user = new User();
    // recoger los datos del request:
    var params = req.body;
  
    if (params.name && params.surname && params.nick && params.email && params.password ){

            user.name = params.name;
            user.surname = params.surname;
            user.nick = params.nick;
            user.email = params.email;
            user.role = 'ROLE_USER';
            user.image = null;

            // Control para evitar registros duplicados:
            User.find({ $or: [
                        {email: user.email.toLowerCase()},
                        {nick: user.nick.toLowerCase()}
            ]}).exec((err,users) => {
                if(err) return res.status(500).send({ message: 'Error en la peticion de usuarios'});
               
                if(users && users.length >= 1){
                    return res.status(404).send({
                        message: 'El usuario que intentas registrar ya existe'
                    })
                }else{
                     // cifrado de contraseña y guarda los datos:
                     bcrypt.hash(params.password, null, null, (err, hash) =>{
                     user.password = hash;

                     user.save((err, userStored) => {
                        if(err) return res.status(500).send({ message: 'Error al guardar el usuario'})
                     
                        if(userStored){
                             res.status(200).send({user: userStored});
                         }else{
                        res.status(404).send({ message: 'No se ha registrado el Usuario'});
                         }

                     });
                  });
                }
            });
           
            
    }else{
        res.status(200).send({
            message:'Debe enviar todos los campos necesarios'
        });
    }    
}
// login
function loginUser(req, res){
    var params = req.body;

    var email = params.email;
    var password = params.password;
    // metodo para buscar registro de datos:
    User.findOne({ email: email}, (err, user) => {
        if(err) return res.status(500).send({ message: 'Error en la petición'});

        if (user){
            bcrypt.compare(password, user.password, (err, check) =>{
                if(check){
                    // manejo de token
                    if(params.gettoken){
                        //generar y devolver un token
                        return res.status(200).send({
                            token: jwt.createToken(user)
                        });

                    }else{
                        // devolver los datos del usuraio visibles
                        // devolver datos de usuario pero primero elimino la propiedad password para no mostrarla
                        user.password = undefined;
                        return res.status(200).send({user});
                    }

                }else{
                   // devolver error 
                   return res.status(404).send({ message: 'El usuario no se ha podido identificar'});
                }
            });
        }else{
            return res.status(404).send({ message: 'El usuario no se ha podido identificar !!'}); 
        }
    });
}

// Conseguir datos de un usuario:
function getUser(req, res){
    /**
     *  Cuando los datos los recibimos mediante url 
     * utilizamos el request.params, si los datos los 
     * recibimos por  medio de un metodo post (por ejemplo)
     * utilizamos request.body.
     */
    var userId = req.params.id;

    // Consulta:
    User.findById(userId, (err, user) =>{
        if(err) return res.status(500).send({ message: 'Error en la petición'});

        if(!user) return res.status(404).send({ message: 'El Usuario no existe'});

        followThisUser(req.user.sub, userID).then((value) =>{
            user.password = undefined;
            return res.status(200).send({
                user, 
                following: value.following, 
                followed: value.followed
            });
        });

    });

}

async function followThisUser(identity_user_id, user_id){
    var following = await Follow.findOne({"user": identity_user_id, "followed":user_id})
         .exec((err, follow)=>{
             if(err) return handleError(err);

             return follow
        });
    var followed = await Follow.findOne({"user": user_id, "followed":identity_user_i})
        .exec((err, follow)=>{
        if(err) return handleError(err);

        return follow
        });
    return {
        following: following,
        followed: followed
    }

}

// devolver un listado de usuarios paginado:
function getUsers(req, res){
    //  Obtengo los datos del usuario logueado:
    var identity_users_id =  req.user.sub;

    var page = 1;
    if(req.params.page){
        page = req.params.page;
    }

    var itemsPerPage = 5;
    // Consulta para que devuelva los datos paginados ordenados por id:
    User.find().sort('_id').paginate(page, itemsPerPage,
         (err, users, total) =>{
            if(err) return res.status(500).send({ message: 'Error en la petición'});

            if(!users) return res.status(404).send({ message: 'No hay usuarios disponibles'});

            folloUserId(identity_users_id).then((value)=>{
                return res.status(200).send({
                    users,
                    users_following: value.following,
                    user_follow_me: value.followed,
                    total,
                    pages: Math.ceil(total/itemsPerPage)
                });
            });

            
    });


}

async function folloUserId(user_id){
  
     var following = await Follow.find({"user":user_id}).select({'_id':0, '__v':0, 'user': 0})
    .exec((err, follows) =>{
        return follows;
    });

    var followed = await Follow.find({"followed":user_id}).select({'_id':0, '__v':0, 'followed': 0})
    .exec((err, follows) =>{
        return follows
    });


    // procesar following ids:
    var following_clean = [];

    following.forEach((follow) => {
        following_clean.push(follow.followed);
    });

    // procesar folowed ids:
    var followed_clean = [];
    followed.forEach((follow) => {
        followed_clean.push(follow.user);
    });

    return {
        following: following_clean,
        followed: followed_clean
    }
}
function getCounters(req, res){
    var userId = req.user.sub;
    if(req.params.id){
        userId = req.params.id;
    }
    getCountFollow(userId).then((value) => {
        return res.satus(200).send(value);
    });

}

async function getCountFollow(user_id){
    var folowing = await Follow.count({"user": user_id}).exec((err, count) =>{
        if(err) return handleError(err);
        return count;
    });

    var folowed = await Follow.count({"followe": user_id}).exec((err, count) =>{
        if(err) return handleError(err);
        return count;
    });

    var publications = await Publication.count({"user": user_id}).exec((err, count) =>{
        if(err) return handleError(err);
        return count;
    });

    return { 
        following: following,
        folowed: follewed,
        publications:publications
    }

}

// Actualizar los datos de usuario:
function updateUser(req, res){
    var userID = req.params.id;
    var update = req.body;

    //borrar la propiedad password:
    delete update.password;

    if(userID != req.user.sub){
        return res.status(500).send({ 
            message: 'Sin autorización para esta solicitud'
        });
    }

    // Realizar la consulta:
    User.findByIdAndUpdate(userID, update, {new:true}, (err, userUpdated) => {
        if(err) return res.status(500).send({ message: 'Error en la petición'});

        if(!userUpdated){
             return res.status(404).send({
                 message: 'No se ha podido actualizar el usuario'
            });
        }

        return res.status(200).send({user: userUpdated});
    });
} 

// Subir archivos de imagen/avatar de Usuario
function uploadImage(req, res){
    var userID = req.params.id;

   
    if(req.files){
        var file_path = req.files.image.path;
        console.log(file_path);

        var file_split = file_path.split('\\');
        console.log(file_split);

        var file_name = file_split[2];
        console.log(file_name);
        
        var ext_split = file_name.split('\.');
        console.log(ext_split);
        var file_ext = ext_split[1];
        console.log(file_ext);


        if(userID != req.user.sub){
            return removeFilesOfUploads(res, file_path, 'Sin autorización para esta solicitud');
        }

        if(file_ext == 'png' || file_ext == 'jpg' ||  file_ext == 'jpeg' ||  file_ext == 'gif'){
            // Actualizar documento de usuario logueado:
            User.findByIdAndUpdate(userID, {image: file_name}, {new:true}, (err, userUpdated) => {
                if(err) return res.status(500).send({ message: 'Error en la petición'});
        
                if(!userUpdated){
                     return res.status(404).send({
                         message: 'No se ha podido actualizar el usuario'
                    });
                }
        
                return res.status(200).send({user: userUpdated});
            });
            
        }else{
           return removeFilesOfUploads(res, file_path, 'Extension no válida');
        }

   }else{
    return res.status(200).send({ message: 'No se han subido imagenes'});  

   }
 
}

function removeFilesOfUploads(res, file_path, message) {
    fs.unlink(file_path, (err) => {
        return res.status(200).send({
            message: message
       });
    });
}

// Devolver imagen de un usuario:
function getImageFile(req, res){
    var image_file = req.params.imageFile;
    var path_file = './uploads/users/'+image_file;

    fs.exists(path_file, (exists) => {
        if(exists){
            res.sendFile(path.resolve(path_file));
        }else{
            res.status(200).send({message:'No existe la imagen...'})
        }
    });
}

// exportar las funciones:
module.exports = {
    home,
    pruebas,
    saveUser,
    loginUser,
    getUser,
    getUsers,
    updateUser,
    uploadImage,
    getImageFile,
    getCounters
}
