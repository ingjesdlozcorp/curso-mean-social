'use strict'
//cargar express
var express = require('express');

//Cargo el modulo de control:
var FollowController = require('../controllers/follow');
// Cargo el router de express:
var api = express.Router();

// cargar los midleware:
var md_auth = require('../midlewares/authenticate');


/*
 como prueba paso el midleware como segundo prametro
 para que se ejecute primero antes del controller:
 */
api.post('/follow', md_auth.ensureAuth, FollowController.saveFollow);
api.delete('/follow/:id', md_auth.ensureAuth, FollowController.deleteFollow);
api.get('/following/:id?/:page?', md_auth.ensureAuth, FollowController.getFollowingUsers);
api.get('/followed/:id?/:page?', md_auth.ensureAuth, FollowController.getFollowedUsers);
api.get('/get-my-follows/:followed?', md_auth.ensureAuth, FollowController.getMyFollows);

module.exports = api;
