'use strict'
//cargar express
var express = require('express');
//Cargo el modulo de control:
var MessageController = require('../controllers/message');
var api = express.Router();
// cargar los midleware:
var md_auth = require('../midlewares/authenticate');

api.get('/probando', md_auth.ensureAuth, MessageController.probando);

api.post('/message', md_auth.ensureAuth, MessageController.saveMessage);

api.get('/my-messages/:page?', md_auth.ensureAuth, MessageController.getReceivedMessages);

api.get('/messages/:page?', md_auth.ensureAuth, MessageController.getEmitMessages);

api.get('/unviewed-messages', md_auth.ensureAuth, MessageController.getUnviewedMessages);

api.get('/set-viewed-messages', md_auth.ensureAuth, MessageController.setViewedMessages);

module.exports = api;