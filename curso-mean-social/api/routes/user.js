'use strict'
//cargar express
var express = require('express');
//Cargo el modulo de control:
var UserController = require('../controllers/user');
//modulo  multipart:
var multipart = require('connect-multiparty');
var api = express.Router();
// cargar los midleware:
var md_auth = require('../midlewares/authenticate');
var md_upload = multipart({uploadDir: './uploads/users'});


api.get('/home', UserController.home);

/*
 como prueba paso el midleware como segundo prametro
 para que se ejecute primero antes del controller:
 */
api.get('/pruebas', md_auth.ensureAuth, UserController.pruebas);

// Registro
api.post('/register', UserController.saveUser);
// Login
api.post('/login', UserController.loginUser);
// Obtener Usuario:
api.get('/user/:id', md_auth.ensureAuth, UserController.getUser);
//obtener usuarios paginados:
api.get('/users/:page?', md_auth.ensureAuth, UserController.getUsers); // '?' parametro page es opcional
// Obtener contadores:
api.get('/counters/:id', md_auth.ensureAuth, UserController.getCounters);
/// actualizar usuario:
api.put('/update-user/:id', md_auth.ensureAuth, UserController.updateUser);
// subir avatar:
api.post('/upload-image-user/:id', [md_auth.ensureAuth, md_upload], UserController.uploadImage);
// devolver imagen de usuario:
api.get('/get-image-user/:imageFile', UserController.getImageFile);


module.exports = api;

