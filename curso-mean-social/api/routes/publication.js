'use strict'

var express = require('express');
var PublicationControler = require('../controllers/publication');
var api = express.Router();
var md_auth = require('../midlewares/authenticate');

var multipart = require('connect-multiparty');
var md_upload = multipart({ uploadDir: './uploads/publications'})

api.post('/publication',md_auth.ensureAuth, PublicationControler.savePublication);

api.get('/publications/:page?',md_auth.ensureAuth, PublicationControler.getPublications);

api.get('/publication/:id',md_auth.ensureAuth, PublicationControler.getPublication);

api.delete('/publication/:id',md_auth.ensureAuth, PublicationControler.deletePublication);

api.post('/upload-image-pub/:id', [md_auth.ensureAuth, md_upload], PublicationControler.uploadImage);
// devolver imagen de usuario:
api.get('/get-image-pub/:imageFile', PublicationControler.getImageFile);


module.exports = api;