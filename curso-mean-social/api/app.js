// aqui se configura todo lo que tiene que ver con express:
'use strict'
// cargar el modulo de express:
var express = require('express');
// modulo para convertir las peticiones en javaScript
var bodyParser = require('body-parser');

// Creacion de variable app haciendo una instancia de express:
var app = express();

// Cargar rutas:
var user_routes = require('./routes/user');
var follow_routes = require('./routes/follow');
var publication_routes = require('./routes/publication');
var message_routes = require('./routes/message');


// middlewares: (metodo que se ejecuta antes de llegar a un controlador)
app.use(bodyParser.urlencoded({extended: false}));// configuración necesaria
app.use(bodyParser.json()); // con esto me convierte las peticiones en objeto Json

// cors: con esto se configuran las cabeceras de forma correcta.

app.use((req, res, next) => {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Headers', 'Authorization, X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Allow-Request-Method');
    res.header('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, DELETE');
    res.header('Allow', 'GET, POST, OPTIONS, PUT, DELETE');
 
    next();
}); 



// rutas:
app.use('/api', user_routes);
app.use('/api', follow_routes);
app.use('/api', publication_routes);
app.use('/api', message_routes);


// exportar configuracion:
module.exports = app;